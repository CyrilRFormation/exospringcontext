package fr.m2i.ExoSpringContext;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import fr.m2i.ExoSpringContext.models.Vehicule;
import fr.m2i.ExoSpringContext.models.Voiture;

@Configuration
public class AppConfiguration {
	
	@Bean
	public Voiture voiture() {
		return new Voiture();
	}
	
	@Bean
	public Vehicule vehicule() {
		return new Vehicule();
	}

}
