package fr.m2i.ExoSpringContext;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import fr.m2i.ExoSpringContext.models.Voiture;

@ComponentScan("fr.m2i.ExoSpringContext")
public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext context = new AnnotationConfigApplicationContext(App.class);
		Voiture vhle1 = context.getBean(Voiture.class);
		vhle1.setCouleur("vert");
		vhle1.setPoids(12.12345f);
		System.out.println(vhle1);
	}
}
