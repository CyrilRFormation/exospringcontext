package fr.m2i.ExoSpringContext.models;

public class Voiture extends Vehicule {

	public Voiture() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String toString() {
		return "Voiture [couleur=" + couleur + ", poids=" + poids + "]";
	}
}
