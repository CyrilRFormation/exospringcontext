package fr.m2i.ExoSpringContext.models;

public class Vehicule {
	public String couleur;
	public Float poids;
	
	public Vehicule() {};
	
	public Vehicule(String couleur, Float poids) {
		super();
		this.couleur = couleur;
		this.poids = poids;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public Float getPoids() {
		return poids;
	}

	public void setPoids(Float poids) {
		this.poids = poids;
	}

	@Override
	public String toString() {
		return "Vehicule [couleur=" + couleur + ", poids=" + poids + "]";
	}
	
	
	
}
